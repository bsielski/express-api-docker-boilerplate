#!/bin/bash

set -o errexit
. env-constants

printf "REPO_LINK=${REPO_LINK}\n" > .env.repo-link

printf "COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME}
DOCKERFILE=${APPSERVER_DOCKERFILE}
CURRENT_USER=${CURRENT_USER}\n" > docker/.env

printf "VIRTUAL_HOST=${VIRTUAL_HOST}\n" > docker/.env.webserver
