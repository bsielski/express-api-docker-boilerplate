#!/bin/bash
set -o errexit
APP_DIR=app

. .env.repo-link
. ./docker/.env

echo "-------------------------------------"
echo "Script 'up.sh', step 1/3"
if [[ ! -d "./$APP_DIR" ]]
then
    echo "'$APP_DIR' directory doesn't exist."
    mkdir $APP_DIR
    echo "'$APP_DIR' directory created."
    git clone $REPO_LINK $APP_DIR
    echo "Repository cloned to $APP_DIR directory."
else
    echo "'$APP_DIR' directory exists."
    git -C $APP_DIR pull
    echo "Repository pulled."
fi

echo "-------------------------------------"
echo "Script 'up.sh', step 2/3"
echo "Install app, start appserver, start webserver..."
CURRENT_USER="${CURRENT_USER}" DOCKERFILE="${DOCKERFILE}" docker-compose -p $COMPOSE_PROJECT_NAME -f docker/docker-compose.yml up -d

echo "-------------------------------------"
echo "Script 'up.sh', step 3/3"
echo "Delete temporary stuff if you want to save a disk space."
docker system prune -a
